include device/rockchip/rk3399/BoardConfig.mk

PRODUCT_PACKAGE_OVERLAYS += device/rockchip/rk3399/ROC_RK3399_PC/overlay
BOARD_SENSOR_ST := true
BOARD_SENSOR_MPU_PAD := false
BUILD_WITH_GOOGLE_GMS_EXPRESS := false
CAMERA_SUPPORT_AUTOFOCUS:= false
BOARD_USE_SPARSE_SYSTEM_IMAGE := false


BOARD_CONNECTIVITY_VENDOR := MediaTek_mt7601
BOARD_CONNECTIVITY_MODULE := mt7601
